package ud6;

import java.util.Scanner;

public class ej10 {

	public static void main(String[] args) {
		/*
			10) Crea un array de n�meros de un tama�o pasado por teclado, el array contendr� n�meros aleatorios primos 
			entre los n�meros deseados,	por �ltimo nos indicar cual es el mayor de todos.
			Haz un m�todo para comprobar que el n�mero aleatorio es primo, puedes hacer todos los m�todos que necesites.
		 */
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Tama�o del array: ");
		int tamano = scan.nextInt();
		
		int[] num = new int[tamano];
		
		num = llenarValores(num);
		
		mostrarArray(num);
		
		scan.close();
		
	}
	
	public static int[] llenarValores (int numero[]) {

		int numeros[]=numero;
		for (int i = 0; i < numeros.length; i++) {
			int num = (int) (Math.random()*50+0);
			if (esPrimo(num)) {
				numeros[i] = num;
				numeros[i] = numero[i];
			}
			
		}

		return(numeros);
	}
	
	
	public static boolean esPrimo(int numero){
		int contador = 2;
		boolean primo=true;
		while ((primo) && (contador!=numero)){
		
			if (numero % contador == 0)
				primo = false;
				contador++;
			}

		return(primo);
		
	}
	
	
	static void mostrarArray (int numero[]) {
		
		for(int i=0;i<numero.length;i++) {
			System.out.print(numero[i] + " ");
		}
		System.out.println();
	}

}
