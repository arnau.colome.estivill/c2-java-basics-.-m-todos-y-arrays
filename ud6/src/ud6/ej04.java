package ud6;

import java.util.Scanner;

public class ej04 {

	public static void main(String[] args) {

		/*
			4) Crea una aplicaci�n que nos calcule el factorial de un n�mero pedido por teclado, lo
			realizara mediante un m�todo al que le pasamos el n�mero como par�metro. Para calcular
			el factorial, se multiplica los n�meros anteriores hasta llegar a uno. Por ejemplo, si
			introducimos un 5, realizara esta operaci�n 5*4*3*2*1=120.
		 */

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Numero: ");
		int numero = scan.nextInt();
		factorial(numero);
		
		scan.close();
	}
	
	public static void factorial(int numero) {
		
		int resultado = 1;
		int cont = numero;
		for(int i = 1; i <= numero; i++) {
		
			resultado = resultado * cont;
			
			System.out.println(cont);
			
			cont--;
		}
		
		System.out.println("El resultado es: " + resultado);
	}

}
