package ud6;

import java.util.Scanner;

public class ej05 {

	public static void main(String[] args) {
		/*
			5) Crea una aplicaci�n que nos convierta un n�mero en base decimal a binario. Esto lo
			realizara un m�todo al que le pasaremos el numero como par�metro, devolver� un String
			con el numero convertido a binario. Para convertir un numero decimal a binario, debemos
			dividir entre 2 el numero y el resultado de esa divisi�n se divide entre 2 de nuevo hasta que
			no se pueda dividir mas, el resto que obtengamos de cada divisi�n formara el numero
			binario, de abajo a arriba.
		*/
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Numero a convertir: ");
		int numero = scan.nextInt();
		
		String binario = convertir(numero);
		
		System.out.println("Numero: " + numero + " en binario correponde a: " + binario);
		
		scan.close();
	}
	
	public static String convertir (int numero) {
		
		StringBuilder binario = new StringBuilder();
		
		int restante;
		
		do {
			restante = (numero % 2);
			numero = numero / 2;
			binario.insert(0, String.valueOf(restante));
		}
		while (numero > 0);
		
		return binario.toString();
	}

}
