package ud6;

import java.util.Scanner;

public class ej01 {

	public static void main(String[] args) {
		/*
			1) Crea una aplicaci�n que nos calcule el �rea de un circulo, cuadrado o triangulo. Pediremos
			que figura queremos calcular su �rea y seg�n lo introducido pedir� los valores necesarios
			para calcular el �rea. Crea un m�todo por cada figura para calcular cada �rea, este devolver�
			un n�mero real. Muestra el resultado por pantalla.
			
			Aqu� te mostramos que necesita cada figura:		
			
			Circulo: (radio^2)*PI
			Triangulo: (base * altura) / 2
			Cuadrado: lado * lado 
		*/
		
		Scanner scan = new Scanner(System.in);
		
		String figura;
		
		System.out.println("De que figura quieres calcular su �rea: ");
		figura = scan.next();
		
		if (figura.equals("Circulo")) {
			System.out.println("Radio del circulo: ");
			double radio = scan.nextDouble();
			Circulo(radio);
		}
		else if (figura.equals("Triangulo")) {
			System.out.println("Base: ");
			double base = scan.nextDouble();
			System.out.println("Altura: ");
			double altura = scan.nextDouble();
			Triangulo(base, altura);
		}
		else if (figura.equals("Cuadrado")) {
			System.out.println("tama�o de los lados");
			double lado = scan.nextDouble();
			Cuadrado(lado);
		}
		else {
			System.out.println("Asegurate que empieza por may�scula");
		}
		
		scan.close();

	}
	
	private static void Circulo(double radio) {
		double area = Math.PI*(Math.pow(radio,2));
		
		area = Math.round(area * 100);
		area = area/100;
		
		System.out.println("La �rea del circulo �s: " + area);
	
	}
	
	private static void Triangulo(double base, double altura) {
		double area = ((base * altura) / 2);
		
		area = Math.round(area * 100);
		area = area/100;
		
		System.out.println("La �rea del triangulo �s: " + area);
	
	}
	
	private static void Cuadrado(double lado) {
		double area = lado * lado;
		
		area = Math.round(area * 100);
		area = area/100;
		
		System.out.println("La �rea del Cuadrado �s: " + area);
	
	}

}
