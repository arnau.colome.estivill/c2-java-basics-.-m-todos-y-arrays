package ud6;

import java.util.Scanner;

public class ej09 {
	public static void main(String[] args) {
		/*
			9) Crea un array de n�meros donde le indicamos por teclado el tama�o del array,
			rellenaremos el array con n�meros aleatorios entre 0 y 9, al final muestra por pantalla el
			valor de cada posici�n y la suma de todos los valores. Haz un m�todo para rellenar el array
			(que tenga como par�metros los n�meros entre los que tenga que generar), para mostrar el
			contenido y la suma del array y un m�todo privado para generar n�mero aleatorio (lo
			puedes usar para otros ejercicios).
		*/
		
		int espacio;
		
		espacio=tamano();
		
		int [] array = new int [espacio];
		
		array = llenarArray(array);
		
		mostrarArray(array);


	}
	
	static int tamano() {
		int espacio;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Dimensio? ");
		espacio = scan.nextInt();
		
		scan.close();
		return (espacio);
	}

	static int[] llenarArray(int[]array) {

		Scanner scan = new Scanner(System.in);
		
		int [] resultado = new int[array.length];
		
		for (int i=0;i<array.length;i++) {
			resultado[i] = (int) Math.floor(Math.random() * 10);
		}
		
		scan.close();
		return(resultado);
	}

	
	static void mostrarArray (int[]array) {
		
		for(int i=0;i<array.length;i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}



}
