package ud6;

import java.util.Scanner;

public class ej07 {

	public static void main(String[] args) {
		/*
			7) Crea un aplicaci�n que nos convierta una cantidad de euros introducida por teclado a otra
			moneda, estas pueden ser a dolares, yenes o libras. El m�todo tendr� como par�metros, la
			cantidad de euros y la moneda a pasar que sera una cadena, este no devolver� ning�n valor,
			mostrara un mensaje indicando el cambio (void).
			El cambio de divisas son:
			
			0.86 libras es un 1 �
			1.28611 $ es un 1 �
			129.852 yenes es un 1 �
		*/
		
		Scanner scan = new Scanner(System.in);

		System.out.println("Cantidad de euros: ");
		Double euros = scan.nextDouble();
		
		System.out.println("moneda a convertir (libras, dolares, yenes): ");
		String moneda = scan.next();
	
		conversor(euros, moneda);
		
		scan.close();
		
	}
	
	public static void conversor(Double euros, String moneda) {
		
		System.out.println("Se convertir�n " + euros + " � a " + moneda);
		
		double resultado=0;
		
		if (moneda.equals("libras")) {
			resultado = euros * 0.86;
		}
		else if (moneda.equals("dolares")) {
			resultado = euros * 1.28611;
		}
		else if (moneda.equals("yenes")) {
			resultado = euros * 129.852;
		}
		else {
			System.out.println("No ha introducido bien la moneda");
		}
		
		resultado = Math.round(resultado * 100);
		resultado = resultado/100;
		
		System.out.println("El total es: " + resultado + " " + moneda);
	}
	
	

}
