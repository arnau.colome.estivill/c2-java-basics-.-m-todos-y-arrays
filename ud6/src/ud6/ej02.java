package ud6;

import java.util.Scanner;

public class ej02 {

	public static void main(String[] args) {
		/*
			2) Crea una aplicaci�n que nos genere una cantidad de n�meros enteros aleatorios que nosotros le pasaremos por teclado. 
			Crea un m�todo donde pasamos como par�metros entre que n�meros queremos que los genere, podemos pedirlas por teclado antes de generar los
			n�meros. Este m�todo devolver� un n�mero entero aleatorio. Muestra estos n�meros por pantalla.
		*/
		Scanner teclat = new Scanner(System.in);
		
		System.out.println("Quantos numeros quieres generar? ");
		int num = teclat.nextInt();
		
		System.out.println("N�mero m�nimo: ");
		int min = teclat.nextInt();
		
		System.out.println("Numero m�ximo: ");
		int max = teclat.nextInt();
		
		for(int i=0; i<num;i++) {
			System.out.println("Numero " + i + " -> " + Aleatorio(min,max));
		}
		
		teclat.close();
		
	}
	
	public static int Aleatorio(int min, int max) {
		
		int num=(int) (Math.random()*(max-1)+min);
		
		return num;
	}

}
