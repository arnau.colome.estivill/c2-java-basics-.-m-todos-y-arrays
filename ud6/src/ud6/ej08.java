package ud6;

import java.util.Scanner;

public class ej08 {

	public static void main(String[] args) {
		/*
			8) Crea un array de 10 posiciones de n�meros con valores pedidos por teclado. Muestra por
			consola el indice y el valor al que corresponde. Haz dos m�todos, uno para rellenar valores y
			otro para mostrar.
		*/

		int[] num = new int[10];

		rellenarValores(num);
		mostrarValores(num);
		
	}
	
	
	public static void rellenarValores (int numero[]) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Introducir valores: ");
		
		for (int i=0; i < numero.length; i++) {
			System.out.println("Numero " + i);
				numero[i] = scan.nextInt();
		}
		scan.close();
	}
	
	public static void mostrarValores (int numero[]) {
		
		System.out.println("Array completado: ");
		
		for (int i=0; i<numero.length; i++) {
			System.out.print(numero[i] + " ");
		}
		System.out.println(" ");
	}



}
