package ud6;

import java.util.Scanner;

public class ej03 {

	public static void main(String[] args) {
		/*
			3) Crea una aplicaci�n que nos pida un n�mero por teclado y con un m�todo se lo pasamos
			por par�metro para que nos indique si es o no un n�mero primo, debe devolver true si es
			primo sino false.
			Un n�mero primo es aquel solo puede dividirse entre 1 y si mismo. Por ejemplo: 25 no es
			primo, ya que 25 es divisible entre 5, sin embargo, 17 si es primo.
		*/
		
		Scanner scan = new Scanner(System.in);

		System.out.println("Escribe un numero: ");
		int numero = scan.nextInt();
		esPrimo(numero);
				
		scan.close();
	}
		
	public static void esPrimo(int numero){
		int contador = 2;
		boolean primo=true;
		while ((primo) && (contador!=numero)){
		
			if (numero % contador == 0)
				primo = false;
				contador++;
			}

		System.out.println(primo);
		
	}


}
