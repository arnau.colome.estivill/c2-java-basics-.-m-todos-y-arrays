package ud6;

import java.util.Scanner;

public class ej06 {

	public static void main(String[] args) {
		/*
			6) Crea una aplicaci�n que nos cuente el n�mero de cifras de un n�mero entero positivo
			(hay que controlarlo) pedido por teclado. Crea un m�todo que realice esta acci�n, pasando
			el n�mero por par�metro, devolver� el n�mero de cifras.
		*/
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Numero: ");
		int num = scan.nextInt();
		
		contador(num);
		
		int cifras = contador(num);
		
		System.out.println("El numero " + num + " tiene " + cifras + " cifras");
		
				
		scan.close();
	}
	
	public static int contador(int numero) {
		int cifras=0;

		while(numero !=0) {
			numero = numero/10;
			cifras++;
		}
		
		return(cifras);
		
	}

}
